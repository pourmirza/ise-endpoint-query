# ISE Endpoint Onboard
This code quries Cicso ISE and provides endpoint details including NAS, IP address, Group, and type.

## Download and Install Python for Windows
Download and install the latest python release from https://www.python.org/downloads/windows/

## Enable the ISE REST APIs 
The ISE REST APIs - also known as External RESTful Services (ERS) - are disabled by default for security. You must enable it:

1. Login to your ISE PAN using the admin or other SuperAdmin user.
2. Navigate to Administration > System > Settings and select ERS Settings from the left panel.
3. Enable the ERS APIs by selecting Enable ERS for Read/Write
4. Do not enable CSRF unless you know how to use the tokens.
5. Select Save to save your changes

## Administrator Groups for REST APIs
The following ISE Administrator Groups allow REST API access:

| ISE Admin | Permissions |
| ------ | ------ |
| SuperAdmin | Read/Write |
| ERSAdmin | Read/Write |
| ERSOperator | Read Only |


You may also map these groups to external Active Directory (AD) groups so you do not need to create local administrators on ISE.

## Create REST API Users
You can use the default ISE admin account for ERS APIs since it has SuperAdmin privileges. However, it is recommended to create separate users with the ERS Admin (Read/Write) or ERS Operator (Read-Onlly) privileges to use the ERS APIs so you can separately track and audit their activities.

1. Navigate to Administration > System > Admin Access
2. Choose Administrators > Admin Users from the left pane
3. Choose  +Add > Create an Admin User to create a new ers-admin and ers-operator accounts.

## Python project and virtual environment
- Download/clone the project
- Go to the project folder
- Create virtual environment
```sh
python -m venv venv
```
- Activate the virtual evironemnt
```sh
venv\Scripts\activate.bat
```
- Install packages
```sh
pip install -r requirements.txt
```
- Run the project
```sh
python ise_endpoint_query.py
```
