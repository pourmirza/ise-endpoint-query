#!/usr/bin/env python
# Import libraries
import re
import csv
import json
import requests
from getpass import getpass
import xmltodict
# suppress certificate warning
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
def valid_mac(macadd):
    mac_reg1 =r"^([0-9A-Fa-f]{2}[:-]?){5}([0-9A-Fa-f]{2})$"
    mac_reg2 = r"^([0-9A-Fa-f]{4}[.]?){2}([0-9A-Fa-f]{4})$"
    if re.findall(mac_reg1, macadd) or re.findall(mac_reg2, macadd):
        return True
    else:
        return False  
def endoint_query(pan_hostname,mnt_hostname,ise_ersadmin,ise_ersadmin_pass):   
    ise_root_cert = False 
    profile_name=""
    group_name=""
    # Enter MAC address
    while True:
        endpoint_mac_address = input('Enter Endpoint MAC address: ').strip()
        if endpoint_mac_address.lower() == "bye":
            return
        if valid_mac(endpoint_mac_address):
            endpoint_mac_address = re.sub('[.:-]', '', endpoint_mac_address)
            endpoint_mac_address = ':'.join([endpoint_mac_address [i : i + 2] for i in range(0, 12, 2)])
            break
        else:
            print()
            print('Invalid MAC Address! Enter a valid MAC address!\n')
    ise_headers = {
        'Accept': "application/json",
        'content-type': "application/json",
        }
    # Get Endpoint ID 
    ise_url = 'https://{}:{}@{}:9060/ers/config/endpoint?filter=mac.EQ.{}'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname, endpoint_mac_address)
    try:
        ise_response = requests.request('GET', ise_url ,verify=ise_root_cert, headers=ise_headers)
    except:
        ise_pan_response = False
    if ise_response.status_code == 200:
        ise_pan_response = True
        try : 
            endpoint_id =  ise_response.json()["SearchResult"]["resources"][0]["id"]
        except:
            ise_pan_response = False
    else:
        ise_pan_response = False
    print()
    print('Sit tight while we query ISE...\n')
    # Get Endpoint Details from PAN Node 
    try:
        ise_url = 'https://{}:{}@{}:9060/ers/config/endpoint/{}'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname, endpoint_id)
        ise_response = requests.request('GET', ise_url,verify=ise_root_cert, headers=ise_headers)
    except:
        pass
    try:
        endpoint_description= ise_response.json()["ERSEndPoint"]["description"]
    except:
        endpoint_description=""
    try:
        profile_id = ise_response.json()["ERSEndPoint"]["profileId"]
    except:
        profile_id = ""
    try:
        group_id = ise_response.json()["ERSEndPoint"]["groupId"]
    except:
        group_id = ""
    # Get Profile name using profile id
    if profile_id:       
        ise_url = 'https://{}:{}@{}:9060/ers/config/profilerprofile/{}'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname, profile_id)
        try:
            ise_response = requests.request('GET', ise_url,verify=ise_root_cert, headers=ise_headers)
        except :
            pass
        if ise_response.status_code == 200:
            profile_name = ise_response.json()["ProfilerProfile"]["name"]
    else: 
        profile_name = "Unknown"   
    # Get group name using group id
    if group_id:
        ise_url = 'https://{}:{}@{}:9060/ers/config/endpointgroup/{}'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname, group_id)
        try:
            ise_response = requests.request('GET', ise_url,verify=ise_root_cert, headers=ise_headers)
        except :
            pass
        if ise_response.status_code == 200:
            group_name = ise_response.json()["EndPointGroup"]["name"]
    else:
        group_name = "Unknown" 

    # API Call to MnT node
    ise_headers = {
        'Accept': "application/xml",
        'content-type': "application/xml",
        }

    #  Search the MNT node database for the latest session that contains the specified MAC address.
    ise_url = 'https://{}:{}@{}/admin/API/mnt/Session/MACAddress/{}'.format(ise_ersadmin, ise_ersadmin_pass, mnt_hostname, endpoint_mac_address)
    try:
        ise_response = requests.request('GET', ise_url,verify=ise_root_cert, headers=ise_headers)
    except :
        ise_mnt_session_response=False
    if ise_response.status_code == 200:
        ise_mnt_session_response= True
        ise_response_json= xmltodict.parse(ise_response.text)
        # print(json.dumps(ise_response_json, indent=4))
    else:
          ise_mnt_session_response=False
    try:
        nas_name = ise_response_json["sessionParameters"]["network_device_name"]
    except:
        nas_name=""
    
    try:
        nas_ip = ise_response_json["sessionParameters"]["device_ip_address"]
    except:
        nas_ip=""
    try:    
        endpoint_ip = ise_response_json["sessionParameters"]["framed_ip_address"]
    except:
        endpoint_ip =""
    try:    
         authentication_method= ise_response_json["sessionParameters"]["authentication_method"]
    except:
        authentication_method =""
    try:
        nas_port= ise_response_json["sessionParameters"]["nas_port_id"]
    except:
        nas_port =""
    try:
        ssid = re.findall("cisco-wlan-ssid=(.*?),", ise_response_json["sessionParameters"]["other_attr_string"])[0]
    except:
        ssid =""
    try:
        vlan_id = re.findall("vlan-id=(.*?),", ise_response_json["sessionParameters"]["other_attr_string"])[0]
    except:
        vlan_id =""
    if not group_name:
        try:
            group_name= ise_response_json["sessionParameters"]["identity_group"]
        except:
            group_name =""

    # End point doesn't exit 
    if not ise_pan_response and not ise_mnt_session_response:
        print("*** MAC address {} hasn't been seen by ISE ***\n".format(endpoint_mac_address))
        endoint_query(pan_hostname,mnt_hostname,ise_ersadmin,ise_ersadmin_pass)  
    else:
        #  Search the MNT node database for the authentication status for all sessions. Last 5 days
        ise_url = 'https://{}:{}@{}/admin/API/mnt/AuthStatus/MACAddress/{}/432000/1/All'.format(ise_ersadmin, ise_ersadmin_pass, mnt_hostname, endpoint_mac_address)
        try:
            ise_response = requests.request('GET', ise_url,verify=ise_root_cert, headers=ise_headers)
        except :
            pass
        if ise_response.status_code == 200:
            ise_response_json= xmltodict.parse(ise_response.text)
            # print(json.dumps(ise_response_json, indent=4))
        if not nas_name:
            try:
                nas_name = ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["network_device_name"]
            except:
                nas_name=""
        try:
            nas_port_type= ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["nas_port_type"]
            if "Wireless" in nas_port_type:
                nas_port_type = "Wireless"    
        except:
                nas_port_type ="" 
        if not ssid:
            try:
                ssid = re.findall("cisco-wlan-ssid=(.*?),", ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["other_attr_string"])[0]
            except:
                ssid =""      
        if not vlan_id:
            try:
                vlan_id = re.findall("vlan-id=(.*?),", ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["other_attr_string"])[0]
            except:
                vlan_id =""
        if not authentication_method:
            try:
                authentication_method = ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["authentication_method"]
            except:
                authentication_method =""
        
        if not nas_port:
            try:
                nas_port= ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["nas_port_id"]
            except:
                nas_port =""
        if not endpoint_ip:
            try:    
                endpoint_ip = ise_response_json["authStatusOutputList"]["authStatusList"]["authStatusElements"]["framed_ip_address"]
            except:
                endpoint_ip =""

        print('#' * 45, 'Endpoint Details Summary', '#' * 53)
        print("        Endpoint MAC Address:          {:^110}".format(endpoint_mac_address))
        if endpoint_ip:
            print("        Endpoint IP Address:           {:^110}".format(endpoint_ip))
        if endpoint_description:
            print("        Description:            {:^117}".format(endpoint_description))
        print("        Endpoint Type:              {:^117}".format(profile_name))
        print("        Endpoint Group:             {:^117}".format(group_name))
        if authentication_method:
            print("        Authentication Method:         {:^110}".format(authentication_method)) 
        if nas_port_type:
            print("        Connection Type:               {:^110}".format(nas_port_type)) 
        if nas_port_type=="Wireless" and ssid:
            print("        SSID:                          {:^110}".format(ssid))
        if vlan_id:
            print("        VLAN:                          {:^110}".format(vlan_id))
        if nas_name:
            print("        Network Device Name:           {:^110}".format(nas_name)) 
        if nas_ip:
            print("        Network Device IP Address:     {:^111}".format(nas_ip))
        if nas_port and nas_port_type!="Wireless":
            print("        Network Device Port:           {:^111}".format(nas_port))
            
        print('#' * 124,'\n') 

        endoint_query(pan_hostname,mnt_hostname,ise_ersadmin,ise_ersadmin_pass)  

def main():
    #Connect to ISE 
    ise_is_reachable =False
    ise_root_cert = False
    pan_hostname =""
    mnt_hostname=""
    ise_ersadmin_pass =""
    ise_ersadmin =""
    print("\n")
    print("*** Enter the requested information or type bye to exit *** \n")
    print()
    while not ise_is_reachable: 
        #ISE PAN
        while not pan_hostname:       
            pan_hostname = input("Enter ISE PAN Hostname: ")
            if pan_hostname.lower() =="bye" :
                return
        #ISE ERS MNT Node
        while not mnt_hostname:
            mnt_hostname = input("Enter ISE MNT Hostname: ")
            if mnt_hostname.lower() =="bye" :
                return
        #ISE ERS username
        while not ise_ersadmin:       
            ise_ersadmin = input("Enter ISE ERS Username: ")
            if ise_ersadmin.lower() =="bye" :
                return
        #ISE ERS password
        while not ise_ersadmin_pass:
            ise_ersadmin_pass = getpass("Enter ISE ERS Password: ")
            if ise_ersadmin_pass.lower() =="bye" :
                return
        print()
        # Version info - PAN Node
        ise_headers = {
            'Accept': "application/json",
            'content-type': "application/json",
            }
        ise_url = 'https://{}:{}@{}:9060/ers/config/adminuser/versioninfo'.format(ise_ersadmin, ise_ersadmin_pass, pan_hostname)
        try:
            ise_response = requests.request('GET', ise_url ,verify=ise_root_cert, headers=ise_headers)  
        except requests.ConnectionError:
            print('!!! ISE PAN Node is not reachable, please try again !!!\n') 
            pan_hostname =""
            continue
        if ise_response.status_code == 200:
            pass
        elif ise_response.status_code == 401:
            print('!!! Invalid Username or Password, please try again !!!\n')
            ise_ersadmin =""
            ise_ersadmin_pass =""
            continue
        # Lists the node version and type - MnT node
        ise_headers = {
            'Accept': "application/xml",
            'content-type': "application/xml",
            }
        ise_url = 'https://{}:{}@{}/admin/API/mnt/Version'.format(ise_ersadmin, ise_ersadmin_pass, mnt_hostname)
        try:
            ise_response = requests.request('GET', ise_url ,verify=ise_root_cert, headers=ise_headers)  
        except requests.ConnectionError:
            print('!!! ISE MnT Node is not reachable, please try again !!!\n') 
            mnt_hostname =""
            continue
        if ise_response.status_code == 200:
            ise_is_reachable =True
        elif ise_response.status_code == 401:
            print('!!! Invalid Username or Password, please try again !!!\n')
            ise_ersadmin =""
            ise_ersadmin_pass =""
            continue
    print("*** ISE Connection Established *** \n")        
    endoint_query(pan_hostname,mnt_hostname,ise_ersadmin,ise_ersadmin_pass) 
    
if __name__ == "__main__":
    main()        



